% -*- TeX:UTF-8:Soft -*-

\documentclass[%
	12pt,USenglish,abstract=on, %
]{scrartcl}

\input{oneovern_preamble}
\usepackage{pdflscape}
\usepackage{caption}


\begin{document}
	
	\doublespacing

\textbf{Additional Analysis of NBD vs. NPD}
	
\section{Introduction}

The key claim of the paper is that investors engage in N\"aive Buying Diversification (NBD) but not N\"aive Portfolio Diversification (NPD). The analysis supporting this claim is presented in Section 5 of the paper, incorporating Figures 7--8 and Tables 6--8. Figure 7 / Table 8 show that investors who hold two stocks and top-up both stocks engage in NBD but not NPD. Figure 8 / Tables 7--8 show that in the sample of all multiple-stock buy-days only very few buy-days (involving any number of stocks) result in NPD, with NBD much more likely. 

Note that these results are not sensitive to the different tolerance levels used to measure $1/N$. That is, we do not require perfect uniformity in \pounds X.XX amounts held in each stock, but present three definitions of ``approximate'' $1/N$ (based on the 0.02, 0.05, 0.10 intervals). This is necessary because individual stocks are indivisible.

However, one criticism of this approach might be that, given total buy-day investment, on some buy-days investors could not achieve NPD (without selling positions) even if they wanted to. For example, an investor holds \pounds1,000 of Stock A and \pounds10,000 of Stock B and on the buy-day has a total investment of \pounds5,000. On this buy-day investor can NBD but cannot NPD. The investor might invest in Stock A in order to get closer to NPD, but cannot get all the way. In this example it is infeasible for the investor to achieve $1/N$ through buy activity alone.

\subsection{Implications}

The implication is that our analysis may underestimate the true extent of investor intentions to NPD (though not overestimate the extent of NBD). One argument in response is that investors could always achieve NPD through some combinations of buy and sell transactions. A counterargument is that requiring a sale transactions is a departure from analysing the pure allocation decision, which is the focus of the paper. Investors may not want to sell strongly performing stocks for a variety of reasons (though of course the disposition effect suggests investors do not want to sell poorly performing stocks). Also, economists tend to think in terms of two-stage budgeting choices (total investment, allocation decision) and in that spirit NPD may be unreachable given the total investment amount. We need to decide on whether feasibility is a problem and the extent to which the argument that investors could achieve NPD through a combination of buy and sell transactions is worth making. 

\section{Analysis of Feasible NPD Days}

If we decide that feasibility is an issue to be addressed then we need some more analysis. To address the feasibility issue we could look at NPD behavior on buy-days and sell-days on which it is feasible for the investor to achieve NPD given the total investment / divestment amount.

\subsection{Feasible NPD Buy-Days}

To do so we restrict to buy-days on which there were at least two existing positions and the resulting end-day portfolio had at least two stocks, and on which the total investment amount was sufficiently large that an allocation to achieve NPD through buying activity only was feasible, and on which there was no sell activity. This sample includes 7,172 of the 25,507 buy-days (28.2\%) in the main analysis in Figure 8 / Tables 7--8. Summary stats for this sample are shown in \ref{tab:reachable_sum}. Investors in this sample have similar tenure,  and trades per month to the main sample, but fewer stocks in the portfolio and lower portfolio values (which is unsurprising given the restrictions). We then calculated the proportion of buy-days on which NPD was achieved (again, results are not sensitive to choice of 0.02, 0.05 or 0.10 interval for defining NPD). 

Results for presented in \ref{tab:feasible_buy}. Overall only 2.0\% of buy-days result in NPD, consistent with the results from the main sample. Of course, we cannot rule out the hypothesis that some investors choose their total investment amount in order to achieve NPD.

\subsection{Feasible NPD Sell-Days}

Analogous to the previous section, in this section we restrict to sell-days on which there were at least two existing positions and the resulting end-day portfolio had at least two stocks, and on which the total sale amount was sufficiently large that an allocation to achieve NPD through selling only activity was feasible, and on which there was no buy activity. Results are presented in \ref{tab:feasible_sell}. Overall only 2.0\% of sell-days result in NPD, consistent with the results from the main sample (11.0\%). Of course, we cannot rule out the hypothesis that some investors choose their total sell amount in order to achieve NPD.

\subsection{Feasible NPD Buy-and-Sell Days}

Finally, we consider days on which investors undertake switching trades (i.e., bought one or more stocks and sold one or more stocks on the same day), and on which there were at least two existing positions and the resulting position had a least two stocks. Results for presented in \ref{tab:feasible_both}. Overall only 1.6\% of buy-and-sell days result in NPD. Again, it is possible that a part of investors did switching trades because they intended to have 1/N end-day portfolio. 

\subsection{Implications}

Can we claim that investors do not try to reach NPD on their buy and sell days? One would think that if investors really wanted to achieve $1/N$ then they would undertake the combination of buy and sell to move to a $1/N$ position. We do not see this in the main sample. If many investors are constrained to the buy-day total investment / divestment amount we see in the data, then the analysis of feasible NPD buy- and sell-days shows that investors do not NPD, suggesting that the total investment / divestment constraint is not the cause of failure to achieve NPD. Is this enough to convince the critical reader that investors do not have NPD as a strategy?

\section{Additional Information on NBD vs non-NBD investors}

There may be interesting differences between NBD and non-NBD investors that might help us to better understand why they do/do-not engage in NBD behavior. We classify an investor as NBD-type if he/she exhibits at least one multiple-stock buy-day with NBD during the sample period, and non-NBD if he/she has no multiple-stock buy-days with NBD during the sample period.

\subsection{Mutual Fund Purchases}

NBD investors have more diversified portfolios because they purchase multiple stocks. Additionally, NBD investors might be even more diversified in their other positions through holding diversified products such as mutual funds and ETFs. If there were the case then their NBD strategy might not alter their overall portfolio balance very much. Say an investor uses ETFs and Mutual Funds for most of their portfolio, and additionally buys some individual stocks $1/N$. The investor's overall portfolio would be very well diversified. We calculated the proportion of NBD vs non-NBD investors who had ever invested in diversified products. The diversified products (using a lookup from Datastream) are: Closed-Ended Fund, Equity Exchange-Traded Fund, Exchange-Traded Commodity, Other Exchange-Traded Fund, or Bond Exchange-Traded Fund. We find very similar proportions of NBD vs. non-NBD investors using these products: 19.0\% for NBD investors and 17.8\% for non-NBD investors.

\subsection{Returns}

NBD investors might also exhibit n\"aivety in the stocks they purchase, such as buying low-return stocks. We compare the returns earned by NBD vs. non-NBD investors. We could calculate returns as Returns =  (All Selling Proceeds + All dividends + Market Value on 2016-07-25 - All Buy Proceeds) / All Buy Proceeds / Years. We limit the sample to investors with new accounts who engage only in buy and sell transactions (e.g. we exclude investors who move positions off the Barclays Investor platform, and accounts which hold short positions). \ref{tab:returns} shows that average returns for NBD investors and non-NBD investors are very similar. In this version of the table we do not incorporate dividends as dividend payments are yet to be incorporated into the dataset (we are working on this).

\section{Alternative Measure of Uniform Investing}

In the current version the paper we measure NBD and NPD using the percentage of the buy-day investment allocated to each stock. If the percentage investments fall within narrow ranges (defined by the 0.02, 0.05 and 0.10 parameters) then the buy-day is said to be a $1/N$. An alternative way to measure $1/N$ allocations on a continuous scale is the Euclidean norm (also referred to as the L2 norm), which is a measure of uniformity of a vector. \ref{buy_norm} illustrates the distribution of the L2 norm for NBD and NPD on the same of all multiple-stock buy-days. We could consider including this measure in some analysis in the main paper if we think it useful. 

\clearpage

\begin{econtable}
	\footnotesize
	\caption{Proportion of Buy-Days with NPD by Number of Stocks, Feasible NPD Sample}
	\label{tab:feasible_buy}
	\begin{tabular}{l c c c c}
		\vspace{0.25cm}	\\	
		\toprule
		\input{tables/1N_port_reachable.tex}
		\bottomrule
	\end{tabular}	
\end{econtable}

\begin{econtable}
	\footnotesize
	\caption{Proportion of Sell-Days with NPD by Number of Stocks, Feasible NPD Sample}
	\label{tab:feasible_sell}
	\begin{tabular}{l c c c c}
		\vspace{0.25cm}	\\	
		\toprule
		\input{tables/1N_port_reachable_sell_days.tex}
		\bottomrule
	\end{tabular}	
\end{econtable}

\begin{econtable}
	\footnotesize
	\caption{Proportion of Buy-and-Sell Days with NPD by Number of Stocks, Feasible NPD Sample}
	\label{tab:feasible_both}
	\begin{tabular}{l c c c c}
		\vspace{0.25cm}	\\	
		\toprule
		\input{tables/1N_port_switch.tex}
		\bottomrule
	\end{tabular}	
\end{econtable}

\clearpage

\begin{econtable}
	\footnotesize
	\caption{Estimated Returns for NBD and non-NBD investors}
	\label{tab:returns}
	\begin{tabular}{l c c c c c c}
		\vspace{0.25cm}	\\	
		\toprule
		\input{tables/returns_NBD_wo_dividends.tex}
		\bottomrule
	\end{tabular}	
\end{econtable}

\clearpage

\begin{figure}
	\centering
	\caption{Euclidean Norm Measure of NBD and NPD on Multiple-Stock Buy-Days}
	\label{buy_norm}
	\vspace{0.5cm} 
	\subfigure[NBD]{
		\includegraphics[width=0.6\textwidth]{figures/L2_norm_buying}%
	}
	\subfigure[NPD]{
		\includegraphics[width=0.6\textwidth]{figures/L2_norm_mv}
	}
\end{figure}

\clearpage

\appendix

\clearpage

\begin{econtable}
	\footnotesize
	\caption{Summary Statistics for Feasible NPD Buy-Days Sample\vspace{0.5cm}}	
	\label{tab:reachable_sum}
	
	\begin{tabular}{l c c c c c c}
		Panel (A) \\
		\toprule
		\input{tables/stats_reachable.tex}
		\bottomrule
	\end{tabular}
	
	\vspace{0.5cm}
	
	\begin{tabular}{l c c c}
		Panel (B) \\
		\toprule
		\input{tables/gender_table_reachable.tex}
		\bottomrule
	\end{tabular}
	
	\vspace{0.5cm}
	
	\begin{tabular}{l c c c c c}
		Panel (C) \\
		\toprule
		\input{tables/ave_num_of_trades_per_month_table_reachable.tex}
		\bottomrule
	\end{tabular}
	
\end{econtable}


\end{document}
