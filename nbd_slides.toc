\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Data}{11}{0}{2}
\beamer@sectionintoc {3}{Na\"ive Buying Diversification}{18}{0}{3}
\beamer@sectionintoc {4}{Sources of Na\"ive Buying Diversification}{29}{0}{4}
\beamer@sectionintoc {5}{Na\"ive Buying Diversification and Narrow Framing}{44}{0}{5}
\beamer@sectionintoc {6}{Na\"ive Buying Diversification and Simplification}{58}{0}{6}
\beamer@sectionintoc {7}{Conclusion}{67}{0}{7}
