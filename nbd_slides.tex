% THEME

\documentclass{beamer}
\usetheme{Boadilla}
\usecolortheme{dolphin}
\usepackage{booktabs}
\setbeamertemplate{caption}[numbered]

\usepackage{subfig}

%TITLEPAGE AND CONTENTS

\title{Na\"ive Buying Diversification and Narrow Framing \\ Among Individual Investors}
\author[shortname]{John Gathergood\inst{1} \and David Hirshleifer\inst{2} \and David Leake\inst{3} \\ \and Hiro Sakaguchi\inst{4} \and Neil Stewart\inst{4}} 
\institute[shortinst]{\inst{1}University of Nottingham \and 
						\inst{2}University of California, Irvine \and
						\inst{3}Lloyds Banking Group \and
						\inst{4}University of Warwick}
\date{March 2019}
\setbeamertemplate{sidebar right}{}
\setbeamertemplate{footline}{%
	\hfill\usebeamertemplate***{navigation symbols}
	\hspace{1cm}\insertframenumber{}/\inserttotalframenumber}

\begin{document}
	
\begin{frame}
\titlepage	
\end{frame}

%%%%%%%%
%% INTRO
%%%%%%%%

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[hideallsubsections]
\end{frame}

\section{Introduction}

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[currentsection, hideallsubsections]
\end{frame}

\begin{frame}{Motivation}
When faced with financial decisions people often adopt simple rules \smallskip
\begin{itemize}
\item Save a fixed fraction of income \smallskip
\item Choose a mortgage contract to keep payments at target value \bigskip
\end{itemize}
\structure{$\Rightarrow$} Heuristics may be poor approximations of (quasi-)rational behavior \\ \bigskip
\structure{$\Rightarrow$} Understanding heuristics is important for realistic models of behavior and for policy design
\end{frame}

\begin{frame}{Motivation}
A common choice faced by investors is how to allocate investments across multiple stocks  \smallskip
	\begin{itemize}
		\item Portfolio theory determines optimal diversification (Markowitz 1952)\smallskip
		\item Evidence shows individual investors deviate substantially by: \smallskip
		\begin{itemize}
			\item (i) Not participating in risky asset markets (Haliassos \& Bertaut 1995) \smallskip
			\item (ii) Under-diversifying when they do participate in risky asset markets (Goetzmann \& Kumar 2008) \smallskip
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Motivation}
One major explanation is narrow framing \smallskip
	\begin{itemize}
		\item Tendency to make choices in isolation instead of an integrated decision (Gilovich \& Griffin 2010) \smallskip
		\item Explains non-participation, under-diversification \smallskip
		\begin{itemize}
			\item Investors fail to appreciate that the stock market does not vary perfectly with other component's of the investor's portfolio, e.g., real estate (Barberis \& Huang 2008) \smallskip		
			\item Investors fail to appreciate that each stock is imperfectly correlated with the rest of the portfolio (Barberis et al. 2006)  \smallskip
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{This Paper}	
Our contribution: \smallskip
	\begin{itemize}
		\item Show that investors commonly group trades on the same day  \smallskip
		\begin{itemize}
			\item 31\% of total invested in our sample due to multiple-stock buy-days \smallskip
		\end{itemize}
		\item Investors commonly split their buy-day investments $1/N$ \smallskip
		\begin{itemize}
			\item Use $1/N$ as a buy rule, not as a portfolio balancing rule \smallskip 
			\item Little evidence of any investors buying to achieve balanced portfolios \smallskip
			\item $1/N$ investors appear to have a preference for simplicity \smallskip
			\begin{itemize}
			\item Choose $1/N$ numerator and denominator to make math simple \bigskip
			\end{itemize}
		\end{itemize}		
	\end{itemize}
\structure{$\Rightarrow$} Investors act narrowly, using a \textit{Na\"ive Buying Diversification} heuristic \\
\bigskip
\structure{$\Rightarrow$} Appear to narrowly frame buy choices separately from portfolio holdings
\end{frame}

\begin{frame}{Related Literature I}
Findings depart from the approach of many behavioural finance models \smallskip
	\begin{itemize}
		\item In many psychology-based behavioural finance models (non-rational) investors try to optimize an overall portfolio \smallskip
		\begin{itemize}
			\item Examples: Barberis \& Huang 2001; Daniel, Hirshleifer, \& Subrahmanyam 2001; Grinblatt \& Han 2005; Li \& Yang 2013; Barberis, Mukherjee, \& Wang 2016 \smallskip
		\end{itemize}
		\item Investors appear to be more concerned with balanced buying \textit{per se} \smallskip
	\end{itemize} 
\end{frame}

\begin{frame}{Related Literature II}
Previous literature on the $1/N$ heuristic  \smallskip
	\begin{itemize}
		\item $1/N$ portfolio balancing heuristic \smallskip
		\begin{itemize}
				\item DeMiguel, Galappi, \& Uppal 2009 \smallskip
				\item Performs well in practice vs. an optimal portfolio strategy \smallskip
		\end{itemize}
			\item Evidence for $1/N$ heuristic  \smallskip
			\begin{itemize}
			\item Benartzi \& Thaler 2001 \smallskip
			\item Contributions to retirement savings plans \smallskip
			\item Choice of contribution rate deeply entangles specific purchase transaction rates to overall portfolio weights \smallskip
			\item Especially given inertia in retirement investing (Madrian \& Shea 2001) \smallskip
			\end{itemize}
		\item We can distinguish between $1/N$ as a \textit{buying strategy} vs. a \textit{portfolio strategy} because investors make discrete trades \smallskip	
	\end{itemize}
\end{frame}

%%%%%%%%%%
%% DATA
%%%%%%%%%%

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[hideallsubsections]
\end{frame}

\section{Data}

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[currentsection, hideallsubsections]
\end{frame}

\begin{frame}{Data}
	Barclays Stockbroking individual investor dataset \smallskip
	\begin{itemize}
		\item Transaction history 182,569 accounts April 2012 -- June 2016 \smallskip
		\item 118,169 make at least one buy transaction during the period \smallskip
		\item Nearly all buys are of  common stock (6\% mutual funds / ETFs) \smallskip
		\item Anonymized individual-level identifiers \smallskip
		\item Settlement prices for individual trades \smallskip
	\end{itemize}
\end{frame}

\begin{frame}{Sample}
Our focus \smallskip
	\begin{itemize}
		\item Days on which investors purchase two or more common stocks \smallskip
		\item Call these multiple-stock buy-days \smallskip
		\item \textbf{All accounts}: 261,585 multiple-stock buy-days by 52,866 investors \smallskip
		\item \textbf{New accounts}: 25,507 multiple-stock buy-days by 8,982 investors \smallskip
		\begin{itemize}
			\item Can reconstruct portfolios on the day (as in Barber \& Odean 2001) \bigskip
		\end{itemize}
	\end{itemize}
	\structure{$\Rightarrow$} Multiple-stock buy-days account for 31\% of total invested in period	
\end{frame}

\begin{frame}{Summary Statistics}
	\begin{table}
		\caption{Summary Statistics: All Accounts}
		\begin{tabular}{l c c c }
			\toprule
			Stocks	& Mean 	& Median  & 75th Pctile \\
			\midrule
			Account Age (months)			&	49.1		&  	34.9	& 77.3   \\
			Buy-Day Investment	&	16,500		&  	7,000	& 15,000  \\
			Trades Per Month	&	1.82		&  	0.74	& 1.60  \\
			\bottomrule
			\tiny
			\textit{Monetary units are British Pounds}
		\end{tabular}
	\end{table}
\end{frame}

\begin{frame}{Summary Statistics}
	\begin{table}
		\caption{Summary Statistics: New Accounts}
		\begin{tabular}{l c c c }
			\toprule
			Stocks	& Mean 	& Median  & 75th Pctile \\
			\midrule
			Account Age	(months)		&	11.1		&  	7.8	& 17.8   \\
			Buy-Day Investment	&	11,500		&  	4,000	& 10,900  \\
			Trades Per Month	&	1.49		&  	0.66	& 1.43  \\
			Portfolio Value		& 60,600 & 17,000 & 46,400 \\
			N Stocks in Portfolio & 8.29 & 5.00 & 10.00 \\
			\bottomrule
			\tiny
			\textit{Monetary units are British Pounds}
		\end{tabular}
	\end{table}
\end{frame}

\begin{frame}{Number of Stocks Purchased on the Buy-Day}
	\label{nstocks}
	\begin{figure}
		\includegraphics[width=0.6\textwidth]{figures/num_purchase_day}
	\end{figure}
	\hyperlink{stockvolatility}{\beamergotobutton{N and Volatility}}
	\hyperlink{stocktime}{\beamergotobutton{N Time Series}}
\end{frame}

%%%%%%%%%%
%% NAIVE DIVERSIFICATION RESULTS
%%%%%%%%%%

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[hideallsubsections]
\end{frame}

\section{Na\"ive Buying Diversification}

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[currentsection, hideallsubsections]
\end{frame}

\begin{frame}{Diversification on Multiple-Stock Buy-Days}
	Diversification choices on buy-days \smallskip
	\begin{itemize}
		\item First, two-stock buy-days \smallskip
		\item Choose one stock at random to be ``Stock A'' \smallskip
		\item Calculate proportion of buy-day investment allocated to Stock A \smallskip
		\item Repeat for $N$-stock buy-days \smallskip
		\begin{itemize}
			\item Choice of stock at random avoids possible dependence of observations \smallskip
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Investment Allocation on Two-Stock Buy-Days}
	\begin{figure}
	\includegraphics[width=0.65\textwidth]{figures/two_purchases_histogram_all_ac}
	\end{figure}
\end{frame}

\begin{frame}{Investment Allocation on Three-Stock Buy-Days}
	\begin{figure}
		\includegraphics[width=0.65\textwidth]{figures/three_purchases_histogram_all_ac}
	\end{figure}
\end{frame}

\begin{frame}{Investment Allocation on Four-Stock Buy-Days}
	\begin{figure}
		\includegraphics[width=0.65\textwidth]{figures/four_purchases_histogram_all_ac}
	\end{figure}
\end{frame}

\begin{frame}{Investment Allocation on Five-Stock Buy-Days}
	\begin{figure}
		\includegraphics[width=0.65\textwidth]{figures/five_purchases_histogram_all_ac}
	\end{figure}
\end{frame}

\begin{frame}{Investment Allocation on Six-Stock Buy-Days}
	\begin{figure}
		\includegraphics[width=0.65\textwidth]{figures/six_purchases_histogram_all_ac}
	\end{figure}
\end{frame}

\begin{frame}{Diversification on Multiple-Stock Buy-Days}
	Many investors engage in \textit{Na\"ive Buying Diversification} \smallskip
	\begin{itemize}
		\item Split investment proportions $1/N$ over $N$ stocks \smallskip
		\begin{itemize}
			\item Mini-peaks, e.g., at 0.33:0.66, 0.66:0.33 \smallskip
		\end{itemize}
		\item Stock indivisibility implies precise $1/N$ may not be possible \smallskip
		\begin{itemize}
			\item [1] For small investment amounts \smallskip
			\item [2] When price of stocks purchased is high \smallskip
			\item Calculate approximate $1/N$ within bandwidths 49\%--51\%, 47.25\%--52.5\%, 45\%--55\% for two-stock case \smallskip
			\item For $N$ stock case, generalize to \pounds $P/N \times (1 \pm X)$, $X=0.02,0.05,0.10$
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Na\"ive Buying Diversification Summary Statistics}
	\begin{table}
		\caption{Multiple-Stock Buy-Days with $1/N$ Allocations}
		\begin{tabular}{l c c c c}
			\toprule
			& & \textit{Bandwidth} & &  \\
			Stocks	& 0.02 	& 0.05  & 0.1 & Obs. \\
			\midrule
			2			&	29.7\%		&  	36.5\%	& 45.6\% & 177193  \\
			3	& 	20.3\%	& 	23.3\%	& 27.8\% & 48896  \\
			4 & 	18.6\%	&	20.9\%	& 23.9\% & 17672 \\
			5	& 	17.5\%	& 	20.1\%	& 22.4\% & 7925  \\
			6+	& 	15.2\%	& 	18.0\%	& 20.0\% & 9899  \\
			All & 	26.3\%	& 	31.8\%	& 39.1\% & 261585 \\
			\bottomrule
		\end{tabular}
	\end{table}
\end{frame}

\begin{frame}{Na\"ive Buying Diversification}
	Is NBD driven mechanically or by recommendation? \smallskip
	\begin{itemize}
		\item E.g., anchoring to default NBD allocation on the investment platform \smallskip
		\begin{itemize}
		\item Or NBD might be encouraged if platform allows grouping of trades \smallskip			
		\end{itemize}
			\item Not relevant on Barclays Stockbroking platform \smallskip
		\begin{itemize}
			\item Each transaction required a separate multiple-screen journey \smallskip
			\item Watch at \url{https://www.youtube.com/watch?v=AsV-pve696M} \smallskip
		\end{itemize} 
	\end{itemize}
\end{frame}


%%%%%%%%%%
%% CHARACTERISTICS RESULTS
%%%%%%%%%%

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[hideallsubsections]
\end{frame}

\section{Sources of Na\"ive Buying Diversification}

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[currentsection, hideallsubsections]
\end{frame}


\begin{frame}{Sources of Na\"ive Buying Diversification }
	Possible sources of variation in NBD \smallskip
	\begin{itemize}
		\item Investor types, e.g., gender and age \smallskip
		\item Learning: account age and trading frequency \smallskip
		\item Limited attention: measures of financial stakes \smallskip
	\end{itemize}
\end{frame}

\begin{frame}{Sources of Na\"ive Buying Diversification }
	Sources of NBD \smallskip
	\begin{itemize}
		\item \textbf{Investor types, e.g., gender and age} \smallskip
			\begin{itemize}
				\item Gender and age differences in trading behavior 
				\begin{itemize}
				\item Choi et al. 2002; Agnew, Balduzzi, \& Sundén 2003; Dorn \& Huberman 2005;
				Mitchell et al. 2006 \smallskip
				\end{itemize}
				\item Male investors with higher confidence might, either \smallskip
				\begin{itemize}
					\item Be less reliant on $1/N$ benchmark, or \smallskip
					\item Tend more to System 1 (heuristic) thinking \smallskip
				\end{itemize} 
			\end{itemize}	
	\end{itemize}
\end{frame}

\begin{frame}{Investor Types: Gender}
	\label{gender}
	\begin{figure}
		\includegraphics[width=0.5\textwidth]{figures/gender_all_ac}
	\end{figure}
	\hyperlink{genderhist}{\beamergotobutton{Histograms}}
\end{frame}

\begin{frame}{Investor Types: Decade of Birth}
	\label{age}
	\begin{figure}
		\includegraphics[width=0.9\textwidth]{figures/year_of_birth_all_ac}
	\end{figure}
   	\hyperlink{agedist}{\beamergotobutton{Distribution}} \hyperlink{agehist}{\beamergotobutton{Histograms}}
\end{frame}

\begin{frame}{\textit{Aside: Day of the Week}}
	\label{day}
	\begin{figure}
		\includegraphics[width=0.7\textwidth]{figures/day_of_week_all_ac}
	\end{figure}
   	\hyperlink{daydist}{\beamergotobutton{Distribution}} \hyperlink{dayhist}{\beamergotobutton{Histograms}}
\end{frame}

\begin{frame}{\textit{Aside: Month of the Year}}
	\label{month}
	\begin{figure}
		\includegraphics[width=1\textwidth]{figures/month_of_year_all_ac}
	\end{figure}
   	\hyperlink{monthdist}{\beamergotobutton{Distribution}} \hyperlink{monthhist}{\beamergotobutton{Histograms}}
\end{frame}

\begin{frame}{Sources of Na\"ive Buying Diversification }
	Sources of NBD \smallskip
	\begin{itemize}
		\item \textbf{Learning: account age and trading frequency} \smallskip
		\begin{itemize}
			\item Existing evidence mixed on effects of learning \smallskip
			\item Investors learn to avoid the disposition effect \smallskip 
			\begin{itemize}
				\item Feng \& Seasholes 2005; Seru, Shumway, \& Stoffman 2010 \smallskip
			\end{itemize}
			\item Investors learn to make mistakes in IPO auctions \smallskip
			\begin{itemize}
				\item Kaustia \& Knupfer 2008; Chiang et al. 2011 \smallskip
			\end{itemize} 
			\item Broader literature on learning and financial products \smallskip 
			\begin{itemize}
			\item Agarwal et al. 2008; Ater \& Landsman 2013; Miravete \& Palacios-Huerta 2014 \smallskip
			\end{itemize}
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Learning: Account Age}
	\label{accage}
	\begin{figure}
		\includegraphics[width=0.6\textwidth]{figures/ac_tenure_all_ac}
	\end{figure}
	\hyperlink{accagehist}{\beamergotobutton{Histograms}}
\end{frame}

\begin{frame}{Learning: Trading Frequency}
	\label{freq}
	\begin{figure}
		\includegraphics[width=0.6\textwidth]{figures/num_monthly_trades_all_ac}
	\end{figure}
	\hyperlink{freqhist}{\beamergotobutton{Histograms}}
\end{frame}

\begin{frame}{Sources of Na\"ive Buying Diversification }
	Sources of NBD \smallskip
	\begin{itemize}
		\item \textbf{Limited attention: measures of financial stakes} \smallskip
		\begin{itemize}
			\item Large literature on attention effects in financial markets
			\begin{itemize}
				\item Dellavigna \& Pollet 2009; Hirshleifer, Lim, \& Teoh 2009; Hou, Peng, \& Xiong 2009; Cohen, Diether, \& Malloy 2013; Huang 2015 \smallskip
			\end{itemize}
			\item Rational inattention models explain inattention to low-stakes decisions as arising due to opportunity cost (Sims, 2003) \smallskip
			\begin{itemize}
			\item Mixed evidence for rational inattention \smallskip				
			\item Dellavigna 2009; Chetty et al. 2014; Taubinsky \&	Rees-Jones 2017; Gathergood et al. 2018 \smallskip
			\end{itemize}
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Limited Attention: Difference in Returns}
	\label{preturns}
	\begin{figure}					
		\includegraphics[width=0.45\textwidth]{figures/past_return_all_ac}
		\includegraphics[width=0.45\textwidth]{figures/next_return_all_ac}
	\end{figure}
	\hyperlink{preturnshist}{\beamergotobutton{Past Return Histograms}}
	\hyperlink{freturnshist}{\beamergotobutton{Future Return Histograms}}
	\hyperlink{industry}{\beamergotobutton{Same vs Different Industry}}
\end{frame}

\begin{frame}{Limited Attention: Portfolio Value and Investment Amount}
	\label{value}
	\begin{figure}					
		\includegraphics[width=0.45\textwidth]{figures/port_value_new_ac}
		\includegraphics[width=0.45\textwidth]{figures/inv_amt_all_ac}
	\end{figure}
	\hyperlink{pvaluehist}{\beamergotobutton{Portfolio Value Histograms}}
	\hyperlink{amounthist}{\beamergotobutton{Investment Amount Histograms}}
\end{frame}

\begin{frame}{Sources of Na\"ive Buying Diversification }
Summary: \smallskip
	\begin{itemize}
		\item Propensity to engage in NBD is invariant to investor age and gender \smallskip
		\item Falls with experience and as stakes rise \smallskip
		\begin{itemize}
			\item Consistent with roles for learning and limited attention \smallskip
			\item But, NBD at least 20\% among top quartiles of buy-days  \smallskip			
		\end{itemize} 

	\end{itemize}
\end{frame}


%%%%%%%%%%
%% NARROW FRAMING
%%%%%%%%%%

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[hideallsubsections]
\end{frame}

\section{Na\"ive Buying Diversification and Narrow Framing}

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[currentsection, hideallsubsections]
\end{frame}

\begin{frame}{Narrow Framing}
How are investors using the $1/N$ rule? \smallskip
	\begin{itemize}
		\item $1/N$ is a useful \textit{portfolio} heuristic \smallskip
		\begin{itemize}
			\item DeMiguel, Galappi, \& Uppal 2009 \smallskip
			\item Easy to implement \smallskip
			\item Performs well in practice vs. attempting to hold the optimal portfolio due to estimation error \smallskip
		\end{itemize}
		\item However, investors appear to be engaging in \textbf{NBD}, not \textit{Na\"ive Portfolio Diversification} (NPD) \smallskip
		\item Explore NBD vs NPD in more detail
	\end{itemize}
\end{frame}

\begin{frame}{Narrow Framing}
``Top-up'' Buy-days \smallskip
	\begin{itemize}
		\item Buy-days where investors add to existing positions in multiple stocks \smallskip
		\item Do investors buy to achieve NBD or NPD? \smallskip
		\item Allow tolerance in definition of NBD and NPD ($X=0.02,0.05,0.10$) \smallskip
		\begin{itemize}
			\item Results are invariant to changes in bandwidth
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{NBD vs. NPD: Top-Up Buy-Days}
	\begin{figure}					
		\includegraphics[width=0.5\textwidth]{figures/two_top_up_histogram_buying}
		\includegraphics[width=0.5\textwidth]{figures/two_top_up_histogram_mv}
	\end{figure}
\end{frame}

\begin{frame}{Narrow Framing}
Sensitivity test \smallskip
	\begin{itemize}
		\item Given value of existing positions and size of investment on the day, a $1/N$ portfolio allocation may be unreachable without \smallskip
		\begin{itemize}
			\item [1] Increasing total amount invested on the day, or \smallskip
			\item [2] Selling existing position(s) \smallskip
		\end{itemize}
		\item These frictions might cause us to \textit{underestimate} NPD \smallskip
		\begin{itemize}  
		\item Restrict to sub-sample that requires neither 1 or 2 \smallskip
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{NDB vs. NPD: Top-Up Buy-Days}
	\begin{itemize}
		\item Additionally restricted sample
	\end{itemize}
	\begin{figure}					
		\includegraphics[width=0.5\textwidth]{figures/two_top_up_histogram_buying_reachable}
		\includegraphics[width=0.5\textwidth]{figures/two_top_up_histogram_mv_reachable}
	\end{figure}
\end{frame}

\begin{frame}{Narrow Framing}
NBD vs NPD on all buy-days (not just top-ups) \smallskip
	\begin{itemize}
		\item Examine allocation on all buy-days involving multiple stocks \smallskip
		\begin{itemize}
			\item [1] With or without existing positions \smallskip
			\item [2] Topping-up positions or buying new stocks \smallskip
		\end{itemize}
		\item Investors commonly achieve NBD but hardly ever NPD \smallskip
	\end{itemize}
\end{frame}

\begin{frame}{NDB vs. NPD: All Buy-Days}
	\begin{figure}					
		\includegraphics[width=0.95\textwidth]{figures/props_1N_port_buy}
	\end{figure}
\end{frame}

\begin{frame}{Narrow Framing}
Summary: Investors aim for NBD, not NPD \smallskip
	\begin{itemize}
		\item Act very narrowly, na\"ively diversifying their buying, not their holdings \smallskip
		\item Little evidence of any investors buying such that they achieve NPD  \smallskip
		\item Investors \textit{appear} to have a buy strategy but not a portfolio strategy \smallskip		
	\end{itemize}
\end{frame}

\begin{frame}{Extension: Selling Behaviour}
	Narrow framing may be less likely in selling behavior \smallskip
	\begin{itemize}
		\item When selling, investors are confronted with portfolio information \smallskip
		\item Disposition and rank effects suggest investors examine portfolios \smallskip
		\begin{itemize}
		\item Barber \& Odean 2013, Hartzmark 2015 \smallskip
		\end{itemize}
		\item \textit{Na\"ive Selling Diversification} may be less common \smallskip		
	\end{itemize}
\end{frame}

\begin{frame}{Extension: Selling Behaviour}
	Selling behaviour \smallskip
	\begin{itemize}
		\item Focus on days on which investors sell multiple-stocks \smallskip
		\begin{itemize}
			\item 15\% of sell-days involve multiple-stocks \smallskip
			\item 62\% are liquidation sales \smallskip
			\item We focus on non-liquidation sales \smallskip
		\end{itemize}
		\item Study allocation of sell amounts across $N$ stocks sold on the day \smallskip
		\begin{itemize}
			\item Choose one stock at random to be ``Stock A'' \smallskip
			\item Calculate proportion of sell-day amount allocated to Stock A \smallskip
		\end{itemize}
		\item Also estimate whether multiple-stock sell-days result in NPD
	\end{itemize}
\end{frame}

\begin{frame}{Na\"ive Selling Diversification}
	\begin{figure}					
		\includegraphics[width=0.65\textwidth]{figures/two_stock_sales_histogram_all_ac}
	\end{figure}
\end{frame}

\begin{frame}{Na\"ive Selling Diversification}
	Na\"ive Selling Diversification  \smallskip
	\begin{itemize}
		\item Present, but not as common as NBD \smallskip
		\begin{itemize}
			\item Occurs on 13\% of two-stock sell-days \smallskip
			\item Occurs on  11\% of all multiple-stock sell-days \smallskip
		\end{itemize}
		\item NPD also uncommon on sell-days \smallskip
		\begin{itemize}
			\item Across all multiple-stock sell-days, 7.2\% result in NPD \smallskip
			\item Across all multiple-stock buy-and-sell-days, 2.8\% result in NPD
		\end{itemize}
	\end{itemize}
\end{frame}

%%%%%%%%%%
%% $1/N$ Simplification
%%%%%%%%%%

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[hideallsubsections]
\end{frame}

\section{Na\"ive Buying Diversification and Simplification}

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[currentsection, hideallsubsections]
\end{frame}

\begin{frame}{Na\"ive Diversification and Simplification}
	Does NBD occur due to preference for simplicity? \smallskip
	\begin{itemize}
		\item Investors appear to be simplicity seeking by how they implement $1/N$  \smallskip
		\begin{itemize}
			\item Choice of denominator and numerator when using the $1/N$ heuristic  \smallskip
			\item Investors choose so that $1/N$ is reduced to a simple division problem 	\smallskip	
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Simplification of $1/N$}
	\begin{itemize}
		\item Distribution of total invested on the buy-day when $N$ = 2
	\end{itemize}
	\begin{figure}					
		\includegraphics[width=0.85\textwidth]{figures/hist_inv_amt_N2345_log_N2}
	\end{figure}
\end{frame}

\begin{frame}{Simplification of $1/N$}
	\begin{figure}					
		\includegraphics[width=0.65\textwidth]{figures/hist_inv_amt_N2345_log_N2} \\
		\includegraphics[width=0.65\textwidth]{figures/hist_inv_amt_N2345_log_N3}
	\end{figure}
\end{frame}

\begin{frame}{Simplification of $1/N$}
	\begin{figure}					
		\includegraphics[width=0.65\textwidth]{figures/hist_inv_amt_N2345_log_N4} \\
		\includegraphics[width=0.65\textwidth]{figures/hist_inv_amt_N2345_log_N5}
	\end{figure}
\end{frame}

\begin{frame}{Na\"ive Diversification and Simplification}
	Results indicate investors simplify their $1/N$ choices \smallskip
	\begin{itemize}
		\item 	Investors act as if jointly determining ``$1$'' and $N$ \smallskip
		\begin{itemize}
			\item \pounds$2,000$ investments associated with $N = 2$ \smallskip
			\item \pounds$3,000$ investments associated with $N = 3$ \smallskip
			\item \pounds$4,000$ investments associated with $N = 2$ \smallskip
		\end{itemize}
		\item Caveat: we do not have exogenous source of variation in denominator or numerator of the calculation \smallskip
	\end{itemize}
\end{frame}

\begin{frame}{Simplification of $1/N$}
	\begin{figure}					
		\includegraphics[width=0.65\textwidth]{figures/mean_inv_amt_on_num_stocks_bought_on_the_day} \\
	\end{figure}
\end{frame}	

\begin{frame}{Na\"ive Diversification and Simplification}
	Do investors buy in fixed amounts per trade? \smallskip
	\begin{itemize}
		\item On aggregate, mean investment per stock constant with $N$  \smallskip	
		\item NBD could arise if investors always buy in fixed amounts per trade \smallskip 
		\item Can reject this hypothesis: \smallskip
		\begin{itemize}
			\item Sample of investors who make at least $ 1 \times $ single-stock buy trade and $1 \times$ multiple-stock buy trade within period \smallskip
			\item Only 2.3\% spend the same amount per trade (10\% bandwidth) \smallskip
		\end{itemize}
	\end{itemize}
\end{frame}

%%%%%%%%%%
%% CONCLUSION
%%%%%%%%%%

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[hideallsubsections]
\end{frame}

\section{Conclusion}

%Contents
\begin{frame}
	\frametitle{Overview} 
	\tableofcontents[currentsection, hideallsubsections]
\end{frame}

\begin{frame}{Conclusion}
	\begin{itemize}
		\item Investors often use a Na\"ive Buying Diversification heuristic \smallskip
		\item NBD decreases as financial stakes and investor experience increase \smallskip
		\begin{itemize}
			\item Consistent with learning and limited attention \smallskip
		\end{itemize}
		\item Investors engage in NBD, not Na\"ive Portfolio Diversification \smallskip
		\item Investors appear to have a preference for simplicity \smallskip
		\begin{itemize}
			\item Choose numerator and denominator to make math simple \smallskip
		\end{itemize} 
	\end{itemize}
\end{frame}

\begin{frame}{Conclusion}
	\begin{itemize}
		\item Investors act surprisingly narrowly in the diversification choices of the stocks they buy \smallskip
		\begin{itemize}
			\item Our results give a suggestive hint that investors do not have a portfolio strategy in mind \smallskip 
			\item While a $1/N$ portfolio strategy can be rationalized in practice, it appears to be very difficult to rationalize a $1/N$ buying strategy \smallskip
		\end{itemize}
		\item Existing studies in behavioral finance do not capture this narrow framing behavior \smallskip		
	\end{itemize}
\end{frame}

%%%%%%%%%%
%% ADDITIONAL SLIDES
%%%%%%%%%%

\begin{frame}{\textit{Additional Slides}}
\end{frame}

\begin{frame}{Number of Stocks Bought and Market Volatility}
\label{stockvolatility}
	\begin{figure}
		\includegraphics[width=0.6\textwidth]{figures/volatility_num_purchases_inc_single}
	\end{figure}
	\hyperlink{nstocks}{\beamerreturnbutton{Back to Summary Statistics}}
\end{frame}

\begin{frame}{Number of Stocks Bought Time Series}
	\label{stocktime}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{figures/num_purchases_per_day_inc_single}
	\end{figure}
	\hyperlink{nstocks}{\beamerreturnbutton{Back to Summary Statistics}}
\end{frame}

\begin{frame}{Investor Types: Gender}
	\label{genderhist}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{figures/two_purchases_histogram_by_gender_all_ac}
	\end{figure}
	\hyperlink{gender}{\beamerreturnbutton{Back to Investor Types}}
\end{frame}

\begin{frame}{Investor Types: Decade of Birth}
	\label{agedist}
	\begin{figure}
		\includegraphics[width=0.6\textwidth]{figures/YoB_all_ac}
	\end{figure}
	\hyperlink{age}{\beamerreturnbutton{Back to Investor Types}}
\end{frame}

\begin{frame}{Investor Types: Decade of Birth}
	\label{agehist}
	\begin{figure}
		\includegraphics[width=0.9\textwidth]{figures/two_purchases_histogram_by_age_all_ac}
	\end{figure}
	\hyperlink{age}{\beamerreturnbutton{Back to Investor Types}}
\end{frame}

\begin{frame}{\textit{Aside: Day of the Week}}
	\label{daydist}
	\begin{figure}
		\includegraphics[width=0.6\textwidth]{figures/DoW_all_ac}
	\end{figure}
	\hyperlink{day}{\beamerreturnbutton{Back to Investor Types}}
\end{frame}

\begin{frame}{\textit{Aside: Day of the Week}}
	\label{dayhist}
	\begin{figure}
		\includegraphics[width=0.9\textwidth]{figures/two_purchases_histogram_by_week_of_day_all_ac}
	\end{figure}
	\hyperlink{day}{\beamerreturnbutton{Back to Investor Types}}
\end{frame}

\begin{frame}{\textit{Aside: Month of the Year}}
	\label{monthdist}
	\begin{figure}
		\includegraphics[width=0.55\textwidth]{figures/YoM_all_ac}
	\end{figure}
	\hyperlink{month}{\beamerreturnbutton{Back to Investor Types}}
\end{frame}

\begin{frame}{\textit{Aside: Month of the Year}}
	\label{monthhist}
	\begin{figure}
		\includegraphics[width=0.5\textwidth]{figures/two_purchases_histogram_by_month_of_year_all_ac}
	\end{figure}
	\hyperlink{month}{\beamerreturnbutton{Back to Investor Types}}
\end{frame}

\begin{frame}{Learning: Account Age}
	\label{accagehist}
	\begin{figure}
		\includegraphics[width=0.6\textwidth]{figures/two_purchases_histogram_by_account_tenure_all_ac}
	\end{figure}
	\hyperlink{accage}{\beamerreturnbutton{Back to Learning}}
\end{frame}

\begin{frame}{Learning: Trading Frequency}
	\label{freqhist}
	\begin{figure}
		\includegraphics[width=0.6\textwidth]{figures/two_purchases_histogram_by_monthly_frequency_all_ac}
	\end{figure}
	\hyperlink{freq}{\beamerreturnbutton{Back to Learning}}
\end{frame}

\begin{frame}{Limited Attention: Difference in Past Returns}
	\label{preturnshist}
	\begin{figure}					
		\includegraphics[width=0.6\textwidth]{figures/two_purchases_histogram_by_diff_past_3m_return_all_ac}
	\end{figure}
	\hyperlink{preturns}{\beamerreturnbutton{Back to Limited Attention}}
\end{frame}

\begin{frame}{Limited Attention: Difference in Future Returns}
	\label{freturnshist}
	\begin{figure}					
		\includegraphics[width=0.6\textwidth]{figures/two_purchases_histogram_by_diff_next_3m_return_all_ac}
	\end{figure}
	\hyperlink{preturns}{\beamerreturnbutton{Back to Limited Attention}}
\end{frame}

\begin{frame}{Limited Attention: Difference in Future Returns}
	\label{freturnshist}
	\begin{figure}					
		\includegraphics[width=0.6\textwidth]{figures/two_purchases_histogram_by_diff_next_3m_return_all_ac}
	\end{figure}
	\hyperlink{preturns}{\beamerreturnbutton{Back to Limited Attention}}
\end{frame}

\begin{frame}{Limited Attention: Same vs Different Industry}
	\label{industry}
	\begin{figure}					
		\includegraphics[width=0.6\textwidth]{figures/same_ind_all_ac}
	\end{figure}
	\hyperlink{preturns}{\beamerreturnbutton{Back to Limited Attention}}
\end{frame}

\begin{frame}{Limited Attention: Investment Amount}
	\label{amounthist}
	\begin{figure}					
		\includegraphics[width=0.6\textwidth]{figures/two_purchases_histogram_by_investment_amt_all_ac}
	\end{figure}
	\hyperlink{value}{\beamerreturnbutton{Back to Limited Attention}}
\end{frame}

\end{document}


