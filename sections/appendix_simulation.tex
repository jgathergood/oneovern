% !Mode:: "TeX:US:UTF-8:Soft"

\appendix
\setcounter{section}{0}

\section{Simulation Analysis}\label{sec:simulation}

In this appendix we describe the simulation analysis presented in \ref{sec:simulation_main}.

\subsection{Asset Price Simulation}

Our interest is in estimating the portfolio performance (here measured by the Sharpe ratio) of NBD vs. NPD. Simulated data are generated according to the one-factor model used in \citet*{DeMiguel2009}. The simulated data consist of $T$-month long monthly returns of $N$ risk assets. Returns of a factor risk asset is drawn from the normal distribution and returns of the remaining $N-1$ risk assets are linked to the return of the factor asset with factor loadings which are unique to each non-factor asset. \ref{eq:R} describes how asset returns are computed. Risk free returns are drawn from $N(0.02, 0.02)$. Note that the parameter values are identical to those used in \citet*{DeMiguel2009}.

\begin{equation}
\label{eq:R}
R_{a,t}=B R_{b,t}+\epsilon_{t}
\end{equation}

where\\
$R_{a,t}$ is a vector of excess returns of $N-1$ (non-factor) risk assets at time $t$.\\
$R_{b,t}$ is an excess return of a factor risk asset at time $t$, following $N(0.08, 0.16)$.\\
$B$ is a vector of factor loadings equally spreading from 0.5 to 1.5.\\
$\epsilon_{t}$ is a vector of noises at time $t$, following $N(0, \Sigma_{t})$.\\
$\Sigma_{t}$ is a diagonal matrix where the diagonal values are drawn from the uniform distribution with support [0.1, 0.3].\\

In the simulation, an investor rebalances the portfolio and invests additional money at monthly frequency. We assume that the investment amount is held constant across months. At each time step (i.e., each month), NPD investors sell and buy risk assets to equalize their market values while NBD investors allocate the new money equally to $N$ assets without selling any. For simplicity, we assume that a unit of assets is completely divisible and assume zero trading fees.\footnote{The number of trades per month tends to be larger for NPD investors than for NBD investors. Given this, commissions might tend to be larger for NPD investors.}

From the $T$-month long price paths for $N$ risk assets, we calculate $T-1$ monthly portfolio returns and a Sharpe ratio for both on NPD and NBD strategies. A simulation is repeated by 1,000 times, resulting in 1,000 Sharpe ratios for each of NPD and NBD strategies.\footnote{In \citet*{DeMiguel2009}, a portfolio return at $t$ is calculated, using investment weights for $N$ assets which are estimated using asset returns in previous $M$ months. (I.e., investors rebalance a portfolio each month using weights estimated in previous $M$-month window.) They repeat the calculation of a portfolio return from $t=M+1$ to $t=T$, resulting in $T-M$ monthly portfolio returns (rolling-sample method). A Sharpe ratio is calculated based on those $T-M$ portfolio returns ($T=24,000$). In our simulation, we do not use the rolling-sample method because we focus on how investors allocate new money over time in addition to an initial investment.} We then conduct a t-test to examine the statistical significance of differences in Sharpe ratios between the two strategies.

The results are shown in Panel A of \ref{tab:sim}.\footnote{The results for NPD very closely align with those reported in Table 6 of \citet*{DeMiguel2009}} The Sharpe ratios are nearly identical within each column, indicating that NPD and NBD do not differ in their performance  in this baseline case.

In \ref{eq:R}, the higher the idiosyncratic risk represented by $\Sigma_{t}$ the lower the correlation among risk assets. In order to examine a case with a higher degree of idiosyncratic risk, we increase the range of the idiosyncratic volatility (i.e., diagonal elements of $\Sigma_{t}$) from [0.1, 0.3] to [0.4, 0.6] and repeat the simulation (high idiosyncratic risk case). Panel B of Table \ref{tab:sim} shows the results. The results show that NPD performs better than NBD in particular when $N$ and/or $T$ are larger. 

Overall, Panels A and B of \ref{tab:sim} together indicate that, in a market condition with a high idiosyncratic risk (i.e., low correlations among assets), NPD outperforms NBD.